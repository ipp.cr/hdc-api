.. HDC documentation master file, created by
   sphinx-quickstart on Wed Nov  8 17:40:40 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HDC's documentation!
===============================

This is the high-level HDC API design description. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   C
   python



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
